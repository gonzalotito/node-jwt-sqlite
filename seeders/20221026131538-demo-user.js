'use strict';

/** @type {import('sequelize-cli').Migration} */
const bcrypt = require("bcrypt");

module.exports = {
  async up (queryInterface, Sequelize) {
    var salt = bcrypt.genSaltSync(10);

    return queryInterface.bulkInsert('Users', [{
      username: 'user1',
      email: 'user1@example.com',
      password: await bcrypt.hash('user1', 10),
      salt: salt,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
