// // Importing modules
// const bcrypt = require("bcrypt");
// const db = require('../models');
// const jwt = require("jsonwebtoken");
const { response } = require('express');
const bcrypt = require('bcrypt');

const User = require('../models/user');



// // Assigning users to the variable User
// const User = db.users;

const index = async (req, res) => {

    const { limit = 5, from = 0 } = req.query;
    const active = { active: true };

    const [total, users] = await Promise.all([
        User.countDocuments(active),
        User.find(active)
            .skip(Number(from))
            .limit(Number(limit))
    ]);

    res.json({
        total,
        users
    });

    // const users = await User.findAll()
    //     .then(data => {
    //         res.send(data);
    //     }).catch(err => {
    //         res.status(500).send({
    //             message: err.message || "Some error occurred"
    //         });
    //     });
    // return users;
};

// const findOne = async (req, res) => {
//     const id = req.params.id;
    
//     await User.findByPk(id)
//        .then(data => {
//             if (data) {
//                 res.send(data);
//             } else {
//                 res.status(404).send({
//                     message: "User not found"
//                 });
//             }
//         }).catch(err => {
//             res.status(500).send({
//                 message: "Some error occurred"
//             });
//         });
// };

const store = async (req, res) => {

    const { username, email, password, role } = req.body;
    const user = new User({ username, email, password, role });

    // encrypt password
    const salt = bcrypt.genSaltSync();
    user.password = bcrypt.hashSync(password, salt);

    // Save to Database
    await user.save();

    res.json({
        user
    });
};

const update = async (req, res) => {

    const { id } = req.params;

    const { _id, password, google, email, ...user } = req.body;
    
    // TODO: validate
    if (password) {
        // encrypt password
        const salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(password, salt);
    }

    const userUpdate = await User.findByIdAndUpdate(id, user);

    res.json({
        userUpdate
    });
};

const destroy = async (req, res) => { 

    const { id } = req.params;

    // Physical removal
    // const user = await User.findByIdAndDelete(id); 

    const user = await User.findByIdAndUpdate(id, { active: false });
    const userAuthenticated = req.user;

    res.json({
        user, 
        userAuthenticated
    });
};



module.exports = {
    index,
    store,
    update,
    destroy
    // findOne
};
