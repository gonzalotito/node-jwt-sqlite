const { response } = require('express');
const bcryptjs = require('bcryptjs');

const User = require('../models/user');
const { generateJWT } = require('../helpers/jwt-generate');
const { googleVerify } = require('../helpers/google-verify');

const signIn = async (req, res) => {

    const { email, password } = req.body;

    try {

        // Check if the email exists
        const user = await User.findOne({ email: email});

        if (!user) {
            return res.status(400).json({ 
                msg: 'User not found' 
            });
        }

        // Check if the user is active
        if (!user.active) {
            return res.status(400).json({ 
                msg: 'User is not active' 
            });
        }

        // Check password
        const validPassword = bcryptjs.compareSync(password, user.password); 
        if (!validPassword) {
            return res.status(400).json({ 
                msg: 'Password is incorrect'  
            });
        }

        // Generate JWT 
        const token = await generateJWT(user.id);

        res.json({
            user,
            token
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            msg: 'Error: ' + error
        });
    }
    
}

const googleSignIn = async (req, res = response) => {

    const { id_token } = req.body;
    
    try {
        const { username, image, email } = await googleVerify(id_token);

        let user = await User.findOne({ email });

        if (!user) {

            const data = {
                username, 
                email, 
                password: ':P',
                image,
                google: true
            };

            user = new User(data);
            await user.save();
        }

        //
        if (!user.active) {
            return res.status(401).json(
                { msg: 'User bloquado' }
            )
        } 

        // Generate JWT 
        const token = await generateJWT(user.id);

        res.json({
            user,
            token
        });
    } catch (error) {
        res.status(400).json({
            msg: 'Google Token is incorrect'
        });
    }

    
};

module.exports = {
    signIn,
    googleSignIn
}

// const checkToken = async (req, res) => {
//     try {
//         const { token } = req.body;

//         if (token) {
//             const decode = jwt.verify(token, process.env.ACCESS_SECRET);

//             return res.status(200).send({
//                 login: true,
//                 data: decode
//             });
//         } else {
//             res.json({
//                 login: false,
//                 data: error,
//             });
//         }

//     } catch (error) {
//         console.log(error);
//     }

// };

