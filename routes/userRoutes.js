const { Router } = require('express');
const { check } = require('express-validator');

// const { validateReqs } = require('../middlewares/validate-reqs');
// const { validateJWT } = require('../middlewares/validate-jwt');
// const { isAdminRole, hasRole } = require('../middlewares/validate-roles');

const { validateReqs, validateJWT, isAdminRole, hasRole } = require('../middlewares');

const { isValidRole, emailExists, existUserById } = require('../helpers/db-validators');
const { index, store, update, destroy } = require('../controllers/userController');

const router = Router();

router.get('/', index);

router.post('/', [
    check('username', 'Username is required.').not().isEmpty(),
    check('password', 'Password min 5 chars.').isLength({ min: 5 }),
    check('email', 'Email is invalid.').isEmail(),
    check('email').custom(emailExists),
    // check('role', 'Role not valid.').isIn(['ADMIN_ROLE', 'USER_ROLE']),
    check('role').custom(isValidRole),
    validateReqs
], store);

router.put('/:id', [
    check('id', 'Not a valid Mongo ID').isMongoId(),
    check('id').custom(existUserById),
    check('role').custom(isValidRole),
    validateReqs
], update);

router.delete('/:id', [
    validateJWT,
    // isAdminRole,
    hasRole('ADMIN_ROLE', 'TEST_ROLE'),
    check('id', 'Not a valid Mongo ID').isMongoId(),
    check('id').custom(existUserById),
    validateReqs
], destroy)

module.exports = router;