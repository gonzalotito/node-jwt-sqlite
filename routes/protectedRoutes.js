const router = require('express').Router();

// Import our authentication middleware function
const verifyToken = require('../middlewares/authMiddleware');

// This is a protected route - we pass in the authMiddleware function to verify the JWT token before returning anything.
router.get('/', verifyToken, (req, res) => {
    res.send(`This is a protected route - your user EMAIL is: ${req.user.email}`);
});

module.exports = router;