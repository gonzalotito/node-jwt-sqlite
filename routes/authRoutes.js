const { Router } = require('express');
const { check } = require('express-validator');

const { validateReqs } = require('../middlewares/validate-reqs');
const { signIn, googleSignIn } = require('../controllers/authController');

const router = Router();

router.post('/login', [
    check('email', 'Email is incorrect format').isEmail(),
    check('password', 'Password is required').not().isEmpty(),
    validateReqs
], signIn);

router.post('/google', [
    check('id_token', 'Id Token is required').not().isEmpty(),
    validateReqs
], googleSignIn);

module.exports = router;

//importing modules
// const express = require('express')
// const authController = require('../controllers/authController')
// const { signUp, signIn, checkToken } = authController
// const saveUser = require('../middlewares/userMiddleware')

// const router = express.Router()

//signup endpoint
//passing the middleware function to the signup
// router.post('/register', saveUser, signUp)

//login route
// router.post('/login', signIn)

// router.post('/verify-token', checkToken);

// module.exports = router;
