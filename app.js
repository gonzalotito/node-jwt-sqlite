// // const express = require('express');
// const cors = require('cors');
// const moment = require('moment');
// const bodyParser = require('body-parser');
// const cookieParser = require('cookie-parser');
// const swaggerUi = require('swagger-ui-express'), swaggerDocument = require('./swagger.json');

// const userRoutes = require('./routes/userRoutes');
// const authRoutes = require('./routes/authRoutes');
// const protectedRoutes = require('./routes/protectedRoutes');

// // Setting up your port
// const PORT = process.env.PORT || 8080

// // Assigning the variable app to express
// // const app = express();

// app.use(
//     express.urlencoded(),
//     cors({
//         origin: '*'
//     })
// );

// // Middleware
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true}));
// app.use(cookieParser());

// // Routes for the user API
// app.use('/api/auth', authRoutes);
// app.use('/api/users', userRoutes);
// app.use('/api/protected', protectedRoutes);

// app.use(
//     '/api-docs',
//     swaggerUi.serve, 
//     swaggerUi.setup(swaggerDocument)
// );

// // Listening to server connection
// app.listen(PORT, () => {
//     console.log(console.log(moment().format("DD/MM/YYYY HH:mm:ss") + `API listening on port! ${ PORT }` ))
// });
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const Server = require('./models/server');

const server = new Server();
server.listen();

