const Role = require('../models/role');
const User = require('../models/user');

const isValidRole = async (name = '') => {
    const existRole = await Role.findOne({ name });
    if (!existRole) {
        throw new Error(`Role ${name} not exists`);
    }
}

const emailExists = async (email = '') => {
    const emailExist = await User.findOne({ email });
    if (emailExist) {
        throw new Error(`Email ${email} already exists`);
    }
}

const existUserById = async (id) => {
    const existUser = await User.findById(id);
    if (!existUser) {
        throw new Error(`Id ${email} not exists`);
    }
}

module.exports = {
    isValidRole,
    emailExists,
    existUserById
}