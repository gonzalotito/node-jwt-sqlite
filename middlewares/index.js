const validateReqs = require('../middlewares/validate-reqs');
const validateJWT = require('../middlewares/validate-jwt');
const validateRoles = require('../middlewares/validate-roles');

module.exports = {
    ...validateReqs,
    ...validateJWT,
    ...validateRoles
}