const jwt = require("jsonwebtoken");

require("dotenv").config();

const verifyToken = (req, res, next) => {
    //Pull the token value from the header on any requests that come in. If the token isn't present, reject access to the route
    // const token = req.body.token || req.query.token || req.headers["x-access-token"] || req.headers["authorization"];
    const bearerHeader = req.headers["authorization"];
    
    if (!bearerHeader) res.status(403).send("A token is required for authentication");

    const token =  bearerHeader.split(' ')[1];

    // if (!token) res.status(403).send("A token is required for authentication");

    //Use jwt.verify() to check if the token is valid. jwt.verify() returns the content of the JWT, which we store in user
    //If token is verified, add the jwt content to the req object as req.user, so we can access the user id in each route
    //If token is not verified, reject the request
    try {
        const decoded = jwt.verify(token, process.env.ACCESS_SECRET);
        req.user = decoded;
        
    } catch (err) {
        res.status(400).send("Invalid credentials");
    }

    return next();
};

module.exports = verifyToken;