const { response, request } = require("express");

const isAdminRole = (req = request, res = response, next) => {

    if (!req.user) {
        return res.status(500).json({ 
            msg: 'You must be validate token first' 
        });
    }

    const { role, username } = req.user;

    if (role !== 'ADMIN_ROLE') {
        return res.status(401).json({ 
            msg: `${username} is not ADMINISTRATOR`
        });
    }

    next();
};

const hasRole = (...roles) => {

    return (req, res = response, next) => {
        
        if (!req.user) {
            return res.status(500).json({ 
                msg: 'You must be validate token first' 
            });
        }

        if (!roles.includes(req.user.role)) {
            return res.status(401).json({ 
                msg: `The service requires one of these roles ${roles}` 
            });
        }

        next();
    };
};

module.exports = {
    isAdminRole,
    hasRole
};