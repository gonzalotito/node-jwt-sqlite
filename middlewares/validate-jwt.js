const { request, response } = require('express');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

const validateJWT = async (req = request, res = response, next) => {

    const token = req.header('x-token');

    if (!token) {
        return res.status(401).json({ 
            msg: 'There is no token in the request'
        });
    }
    
    try {
        const { uid } = jwt.verify(token, process.env.ACCESS_SECRET);
        
        const user = await User.findById(uid);

        // Check if the uid is active
        if (!user) {
            return res.status(401).json({ 
                msg: 'User not exist in the database'
            });
        }

        // Check if the uid is active
        if (!user.active) {
            return res.status(401).json({ 
                msg: 'User is inactive'
            });
        }
        
        req.user = user;

        next();
    } catch (error) {
        console.log(error);
        res.status(401).json({
            msg: 'Token invalid or expired'
        });
    }

};

module.exports = {
    validateJWT
}