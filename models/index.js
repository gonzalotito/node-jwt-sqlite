// importing modules
const { Sequelize, DataTypes } = require('sequelize');

//Database connection with dialect of postgres specifying the database we are using
const sequelize  = new Sequelize({
    dialect:'sqlite',
    storage: './data/database.sqlite3',
});

//checking if connection is done
sequelize.authenticate()
    .then((result) => {
        console.log('Database connected');
    })
    .catch((err) => {
        console.log('Unable to connect to db: ', err);
    });

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

// connecting to model
db.users = require('./user') (sequelize, DataTypes);

module.exports = db;
