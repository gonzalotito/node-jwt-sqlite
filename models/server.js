const express = require('express');
const cors = require('cors');
const { dbConnection } = require('../database/config');

class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT || 8080;

        this.usersPath = '/api/users';
        this.authPath = '/api/auth';

        // Connect to the Database
        this.connectDB();

        // Middlewares
        this.middlewares();

        // Routes
        this.routes();
    }

    async connectDB() {
        await dbConnection();
    }

    middlewares() {

        // CORS
        this.app.use(cors());

        // Body Parse
        this.app.use(express.json());

        // Public directory
        this.app.use(express.static('public'));
    }

    routes() {
        // this.app.get('/api', (req, res) => {
        //     res.send('Hola Mundo');
        // });
        this.app.use(this.authPath, require('../routes/authRoutes'));
        this.app.use(this.usersPath, require('../routes/userRoutes'));
    }

    listen() {
        this.app.listen(this.port, () => {
            // moment().format("DD/MM/YYYY HH:mm:ss")
            console.log(`API listening on port! ${ this.port }`);
        });
    }
}

module.exports = Server;