// models/user.js
module.exports = (sequelize, DataTypes) => {
    const Report = sequelize.define('report', {
        depto: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        batch_id: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        batch_code: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        proceso_id: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        usuario: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        fecha_proceso: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        fecha: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        cantidad: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        proceso: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
    })

    return Report;
}
