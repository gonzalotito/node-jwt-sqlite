// models/user.js
const { Schema, model } = require('mongoose');

const UserSchema = Schema({
    username: {
        type: String,
        required: [true, 'El nombre de usuario es obligatorio']
    },
    email: {
        type: String,
        required: [true, 'El correo electronico es obligatorio'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    image: {
        type: String
    },
    role: {
        type: String,
        required: true,
        default: 'USER_ROLE',
        enum: ['ADMIN_ROLE', 'USER_ROLE']
    },
    active: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false
    },
});

UserSchema.methods.toJSON = function() {
    const { __v, password, _id, ...user } = this.toObject();
    user.uid = _id;
    return user;
}

module.exports = model('User', UserSchema);








// module.exports = (sequelize, DataTypes) => {
//     const User = sequelize.define('user', {
//         username: {
//             type: DataTypes.STRING,
//             allowNull: false,
//         },
//         email: {
//             type: DataTypes.STRING,
//             allowNull: false,
//         },
//         password: {
//             type: DataTypes.STRING,
//             allowNull: false,
//         },
//         salt: {
//             type: DataTypes.STRING,
//             allowNull: false,
//         },
//         token: {
//             type: DataTypes.TEXT,
//             allowNull: true,
//         },
//         createdAt: {
//             type: DataTypes.DATE,
//             allowNull: true,
//         },
//         updatedAt: {
//             type: DataTypes.DATE,
//             allowNull: true,
//         },
//     })

//     return User;
// }
