module.exports = {
    up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('Reports', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        depto: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        batch_id: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        batch_code: {
          type: DataTypes.STRING,
            allowNull: false,
        },
        proceso_id: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        usuario: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        fecha_proceso: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        fecha: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        cantidad: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        proceso: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: true,
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: true,
        }
      });
    },
    down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('Reports');
    }
  };