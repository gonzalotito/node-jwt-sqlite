const mongoose = require('mongoose');

const dbConnection = async () => {

    try {

        await mongoose.connect(process.env.MONGODB_CONNECTION);

        console.log('Base de datos conectado');

    } catch (err) {
        console.log(err);
        throw new Error('Error al iniciar la base de datos: ' + err.message);
    }

};

module.exports = {
    dbConnection
}